#!/bin/bash

queue=normal_io
config=configuration.txt

# to run directly
# cmd="HtX-offline "

# to run on batch
cmd="bsub -q ${queue} ./runOffline.job "

#data
${cmd} ${config}" inputs/data.txt '-o' SampleName data SampleType Data Normalisation none"

#ttbar
# ${cmd} ${config}" inputs/ttbar.txt '-o' SampleName ttbar"
# ...
