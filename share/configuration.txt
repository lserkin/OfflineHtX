##################################################
#    Configuration file for ttH-offline          #
##################################################
# Notes:                                         #
#             				         #
# Hash demarks the start of a comment            #
# All settings are "CONFIG param"                #
# Settings can be altered with command line args #
#  -o CONFIG param                               #
# All settings need to be predefined in the code #
##################################################

###Where to save everything
OutputDirectory /gpfs/atlas/public/HtX4tops_Ntuples_2.4.22-4/flatNtup_00-00-01_L2
SampleName test

###Sample options
SampleType MC
UseLargeJets true   # in this case these are the RC jets!

###Which systematics to save, provide a full list
Systematics nominal
###Or provide a file
SystematicsFromFile false
SystematicsFile None

###Calculate a normalisation weight from
###totalEventsWeighted,Cutflow or none
Normalisation HtX

###Provide a file to configure the weights to save
WeightsFile weights.txt

###Can choose to provide a list of variables to decorate onto the event
VariableListToDecorate VariablesToRead.txt

###Save all variables decorated onto the event
SaveAllDecorations some
###Otherwise save variables listed in file
VariablesToSaveFile VariablesToSave.txt

SaveDefaultVariables false

###Define the regions of the form Name:"cut expression",Name2:"cut expression"
#Regions 4j3b:"nJets >= 4 && nBTags == 3",4j4b:"nJets >= 4 && nBTags >= 4"
Regions ge4jge0b:"jets_n >= 4"

BTaggingWP 77

##################################################
# TOOL configuration				 #
#      						 #
# Notes:					 #
# 						 #
# Tools in ToolManager have a given setting key  #
# Set key to true to run the tool                #
# Additional options may also be set here        #
##################################################

#use true to turn a tool on
#MyTool true
#AdditionalToolOption this

HtXAnalysis.TurnOn true

#Turn on the MVAVariables tool used for variable calculations
MVAVariables.TurnOn false
#Which working points to enable
MVAVariables.bTagWPs 70
