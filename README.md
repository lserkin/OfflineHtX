How to setup the code:
----------------------

1) Setup AnalysisTop-2.4.23:

    mkdir AnalysisTop_2.4.26
    cd AnalysisTop_2.4.26
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    export ROOTCORE_NCPUS="$(nproc)"
    rcSetup Top,2.4.26
    
2) Check out the OfflineHtX code:

    git clone ssh://git@gitlab.cern.ch:7999/htx/OfflineHtX.git
    
3) Get OfflineTTHbb (trunk), following instructions on (skipping "Check-out the latest tag"):

    https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/OfflineTTHbb#Check_out_the_trunk
    
4) Add to TTHbbConfiguration/Root/GlobalConfiguration.cxx:

    registerSetting("HtXAnalysis.TurnOn", "Set to true to switch on the HtXAnalysis", "false");
    registerSetting("TtTRF.TurnOn", "Set to true to switch on the ttTRF evaluation (HtX)", "false");
    
5) Get the package CustomTRF:

    git clone ssh://git@gitlab.cern.ch:7999/pinamont/CustomTRF.git

6) Compile the code (from the main AnalysisTop directory):

    rc find_packages && rc compile
    
7) Give a look at OfflineHtX/share/* for how to run the code. The main executable is:

    HtX-offline <configuration-file> <inpust-files-list> -o [options]
