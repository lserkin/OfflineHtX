#include "OfflineHtX/OfflineHtXDictionary.h"
#include "OfflineHtX/HtXAnalysis.h"
#include "OfflineHtX/TtTRF.h"

void TTHbb::OfflineHtXDictionary::buildDictionary(TTHbb::ToolManager * toolManager){

  toolManager->addToolToDict("HtXAnalysis.TurnOn",
                             "HtXAnalysis",
                             []{ return new HtXAnalysis("HtXAnalysis");}
                             );
  toolManager->addToolToDict("TtTRF.TurnOn",
                             "TtTRF",
                             []{ return new TtTRF("TtTRF");}
                             );
} 
