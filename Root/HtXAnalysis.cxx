#include "OfflineHtX/HtXAnalysis.h"
#include <iostream>

namespace TTHbb{

  HtXAnalysis::HtXAnalysis(std::string name) : ToolBase(true){
    m_name = name;
  }

  void HtXAnalysis::apply(TTHbb::Event* event){
    
    // set the proper data period (for triggers)
    int run_number = event->intVariable("run_number");
    m_data2015 = false;
    m_data2016 = false;
    if      ( run_number <= 290000 )  m_data2015 = true;
    else if ( run_number >= 290000 )  m_data2016 = true;

    // check electron trigger
    bool pass_HLT_e24_lhmedium_L1EM20VH      = event->intVariable("pass_HLT_e24_lhmedium_L1EM20VH")>0;
    bool pass_HLT_e26_lhtight_nod0_ivarloose = event->intVariable("pass_HLT_e26_lhtight_nod0_ivarloose")>0;
    bool pass_HLT_e60_lhmedium               = event->intVariable("pass_HLT_e60_lhmedium")>0;
    bool pass_HLT_e60_lhmedium_nod0          = event->intVariable("pass_HLT_e60_lhmedium_nod0")>0;
    bool pass_HLT_e120_lhloose               = event->intVariable("pass_HLT_e120_lhloose")>0;
    bool pass_HLT_e140_lhloose_nod0          = event->intVariable("pass_HLT_e140_lhloose_nod0")>0;
    m_trigger_el = false;
    if (m_data2015 && (pass_HLT_e24_lhmedium_L1EM20VH || pass_HLT_e60_lhmedium || pass_HLT_e120_lhloose))                 m_trigger_el = true;
    if (m_data2016 && (pass_HLT_e26_lhtight_nod0_ivarloose || pass_HLT_e60_lhmedium_nod0 || pass_HLT_e140_lhloose_nod0 )) m_trigger_el = true;
    
    // check muon trigger
    bool pass_HLT_mu20_iloose_L1MU15 = event->intVariable("pass_HLT_mu20_iloose_L1MU15")>0;
    bool pass_HLT_mu26_ivarmedium    = event->intVariable("pass_HLT_mu26_ivarmedium")>0;
    bool pass_HLT_mu50               = event->intVariable("pass_HLT_mu50")>0;
    m_trigger_muon = false;
    if (m_data2015 && (pass_HLT_mu20_iloose_L1MU15 || pass_HLT_mu50) ) m_trigger_muon = true;
    if (m_data2016 && (pass_HLT_mu26_ivarmedium || pass_HLT_mu50) )    m_trigger_muon = true;
    
    // count good leptons
    int nGoodEl = 0;
    int nGoodMuons = 0;
    std::vector<std::shared_ptr<Lepton> > m_goodElectrons; m_goodElectrons.clear();
    std::vector<std::shared_ptr<Lepton> > m_goodMuons;     m_goodMuons    .clear();
    //
    unsigned int leptons_n = event->m_leptons.size();
    for(unsigned int i = 0; i < leptons_n; i++){
      std::shared_ptr<Lepton> lep = event->m_leptons.at(i);
      TTHbb::leptonType lepType = lep->type();
      //
      // electrons
      if(lepType==leptonType::electron){
        bool electrons_isSignal = lep->intVariable("electrons_isSignal")>0;
        bool electrons_passOR   = lep->intVariable("electrons_passOR")>0;
        if (lep->Pt() > 28 && abs(lep->Eta() ) < 2.47  && electrons_passOR && electrons_isSignal){
          m_goodElectrons.push_back(lep);
          nGoodEl ++;
        }
      }
      //
      // muons
      else if(lepType==leptonType::muon){
        bool muons_isSignal = lep->intVariable("muons_isSignal")>0;
        bool muons_passOR   = lep->intVariable("muons_passOR")>0;
        if (lep->Pt() > 27 && abs(lep->Eta() ) < 2.47  && muons_passOR && muons_isSignal){
          m_goodMuons.push_back(lep);
          nGoodMuons ++;
        }
      }
    }
    
    //
    // channels & trigger matching
    m_ljets   = false;
    m_ejets   = false;
    m_mujets  = false;
    m_trgMatch = false;
    m_trgMatch_el = false;
    m_trgMatch_mu = false;

    // l+jets selection
    if((nGoodEl+nGoodMuons)==1){
      m_ljets = true;
      std::shared_ptr<Lepton> lep;
      //
      // channel
      if(nGoodEl==1){
	lep = m_goodElectrons[0];
	if (m_trigger_el){
	  m_ejets = true;
	}
      }
      else{
	lep = m_goodMuons[0];
	if(m_trigger_muon){
	  m_mujets = true;
	}
      }
      //
      // trigger matching
      if(m_data2015){
	bool electrons_trgMatch_HLT_e24_lhmedium_L1EM20VH = lep->intVariable("electrons_trgMatch_HLT_e24_lhmedium_L1EM20VH")>0;
	bool electrons_trgMatch_HLT_e60_lhmedium          = lep->intVariable("electrons_trgMatch_HLT_e60_lhmedium")>0;
	bool electrons_trgMatch_HLT_e120_lhloose          = lep->intVariable("electrons_trgMatch_HLT_e120_lhloose")>0;
	bool muons_trgMatch_HLT_mu20_iloose_L1MU15        = lep->intVariable("muons_trgMatch_HLT_mu20_iloose_L1MU15")>0;
	bool muons_trgMatch_HLT_mu50                      = lep->intVariable("muons_trgMatch_HLT_mu50")>0;
	if (electrons_trgMatch_HLT_e24_lhmedium_L1EM20VH || electrons_trgMatch_HLT_e60_lhmedium || electrons_trgMatch_HLT_e120_lhloose){
	  m_trgMatch_el = true;
	}
	if (muons_trgMatch_HLT_mu20_iloose_L1MU15 || muons_trgMatch_HLT_mu50){
	  m_trgMatch_mu = true;
	}
      }
      else if(m_data2016){
	bool electrons_trgMatch_HLT_e26_lhtight_nod0_ivarloose = lep->intVariable("electrons_trgMatch_HLT_e26_lhtight_nod0_ivarloose")>0;
	bool electrons_trgMatch_HLT_e60_lhmedium_nod0          = lep->intVariable("electrons_trgMatch_HLT_e60_lhmedium_nod0")>0;
	bool electrons_trgMatch_HLT_e140_lhloose_nod0          = lep->intVariable("electrons_trgMatch_HLT_e140_lhloose_nod0")>0;
	bool muons_trgMatch_HLT_mu26_ivarmedium                = lep->intVariable("muons_trgMatch_HLT_mu26_ivarmedium")>0;
	bool muons_trgMatch_HLT_mu50                           = lep->intVariable("muons_trgMatch_HLT_mu50")>0;
	if (electrons_trgMatch_HLT_e26_lhtight_nod0_ivarloose || electrons_trgMatch_HLT_e60_lhmedium_nod0 || electrons_trgMatch_HLT_e140_lhloose_nod0){
	  m_trgMatch_el = true;
	}
	if (muons_trgMatch_HLT_mu26_ivarmedium || muons_trgMatch_HLT_mu50){
	  m_trgMatch_mu = true;
	}
      }
      m_trgMatch = m_trgMatch_el || m_trgMatch_mu;
    }
    
    // Build variables
    TLorentzVector neutrino(0,0,0,0);
    neutrino.SetPtEtaPhiE(event->met_met, 0, event->met_phi, event->met_met);
    if(m_ejets)       m_mtw = sqrt(2. * m_goodElectrons[0]->Pt() * event->met_met * (1. - cos( m_goodElectrons[0]->DeltaPhi(neutrino)) ) );
    else if(m_mujets) m_mtw = sqrt(2. * m_goodMuons    [0]->Pt() * event->met_met * (1. - cos( m_goodMuons    [0]->DeltaPhi(neutrino)) ) );    
    if(event->met_met + m_mtw > 60.0) m_pass_triangle = true;
    else                              m_pass_triangle = false;
    m_meff    = 0.;
    m_HT_jets = 0.;
    m_nBTags  = 0;
    m_meff += event->met_met;
    if(m_ejets)       m_meff += m_goodElectrons[0]->Pt();
    else if(m_mujets) m_meff += m_goodMuons    [0]->Pt();
    
    // Loop on jets
    unsigned int jets_n = event->m_jets.size();
    for(unsigned int i = 0; i < jets_n; i++){
      std::shared_ptr<Jet> jet = event->m_jets.at(i);
      m_meff    += jet->Pt();
      m_HT_jets += jet->Pt();
//       if(jet->intVariable("jets_isb_77")){ // this is currently buggy: can be used for ntuples > 2-4-24-1
      if(jet->floatVariable("mv2")>0.645925){
        m_nBTags ++;
      }
    }
    
    // count HOT jets
    m_nHOT = 0;
    unsigned int ljets_n = event->m_largeJets.size();
    for(unsigned int i = 0; i < ljets_n; i++){
      std::shared_ptr<LargeJet> jet = event->m_largeJets.at(i);
      if(jet->intVariable("rc_R10PT05_jets_nconst")<2) continue;
      if(jet->floatVariable("rc_R10PT05_jets_m")<100) continue;
      if(abs(jet->Eta())>2) continue;
      if(jet->Pt()<300) continue;
      m_nHOT ++;
    }
    
    /// Interface with your tool here, by passing it the objects it needs
    /// This is where you can then modify variables decorated to the event, or change them
    event->intVariable("trigger_el")    = m_trigger_el;
    event->intVariable("trigger_muon")  = m_trigger_muon;
    event->intVariable("ljets")         = m_ljets;
    event->intVariable("ejets")         = m_ejets;
    event->intVariable("mujets")        = m_mujets;
    event->intVariable("trgMatch")     = m_trgMatch;
    event->intVariable("trgMatch_el")     = m_trgMatch_el;
    event->intVariable("trgMatch_mu")     = m_trgMatch_mu;
    event->intVariable("pass_triangle") = m_pass_triangle;
    event->floatVariable("meff")        = m_meff;
    event->floatVariable("mtw")         = m_mtw;
    event->floatVariable("HT_jets")     = m_HT_jets;
    event->intVariable("nBTags")        = m_nBTags;
    event->intVariable("nJets")         = jets_n;
    event->intVariable("nHOT")          = m_nHOT;
    event->floatVariable("met")         = event->met_met;
    if(m_ejets)  event->floatVariable("lepton_pt")       = m_goodElectrons[0]->Pt();
    else if(m_mujets) event->floatVariable("lepton_pt")  = m_goodMuons[0]    ->Pt();
    else event->floatVariable("lepton_pt")               = 0.;
    if(m_ejets)  event->floatVariable("lepton_eta")      = m_goodElectrons[0]->Eta();
    else if(m_mujets) event->floatVariable("lepton_eta") = m_goodMuons[0]    ->Eta();
    else event->floatVariable("lepton_eta")              = 0.;
  }
} 
