#ifndef _TTHBB_HTXANALYSIS_H_
#define _TTHBB_HTXANALYSIS_H_

#include "TTHbbToolManager/ToolBase.h"

namespace TTHbb {

  class HtXAnalysis : public ToolBase{

  public:
    HtXAnalysis(std::string name="");
    ~HtXAnalysis(){};

    void initialise(){};
    void apply(TTHbb::Event* event);
    void finalise(){};
    
  private:
    bool m_data2015;
    bool m_data2016;
    
    bool m_trigger_muon;
    bool m_trigger_el;
    
    bool m_ljets;
    bool m_ejets;
    bool m_mujets;
    bool m_ee;
    bool m_mumu;
    bool m_emu;
    bool m_zerolep;

    bool m_trgMatch;
    bool m_trgMatch_el;
    bool m_trgMatch_mu;
    
    bool m_pass_triangle;
    
    float m_mtw;
    float m_meff;
    float m_HT_jets;
    float m_nBTags;
    float m_nHOT;
    
  };
}

#endif 
