#ifndef _TTHBB_TTTRF_H_
#define _TTHBB_TTTRF_H_

#include "TTHbbToolManager/ToolBase.h"
#include "CustomTRF/TtTRFapplication.h"

namespace TTHbb {

  class TtTRF : public ToolBase{

  public:
    TtTRF(std::string name="");
    ~TtTRF(){};

    void initialise(){};
    void apply(TTHbb::Event* event);
    void finalise(){};
    
  private:

  };
}

#endif 
